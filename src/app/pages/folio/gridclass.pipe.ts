import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'gridClass'
})
export class GridClass implements PipeTransform {
  gridClasses = ['horizontal', 'medium', 'medium', 'vertical'];
  itemClass = 'item';

  transform(image, index) {
    return this.gridClasses[index] !== undefined
      ? this.itemClass + ' ' + this.gridClasses[index]
      : this.itemClass;
  }
}
