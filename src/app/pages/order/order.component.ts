import { Component, OnInit } from '@angular/core';
import { CartService } from '../../share/services/cart.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { OrderService } from './order.service';
import { Subscription } from 'rxjs';
import { CustomValidator } from '../../base/custom-validator';
import { Router } from '@angular/router';

@Component({
  selector: 'ln-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss']
})
export class OrderComponent implements OnInit {
  deliveryType = 1;
  address = 1;
  deliveryPrice = 15000;
  totalPrice: number;
  totalPriceOrigin: number;
  totalQuantity: number;
  form: FormGroup;
  confirmed = false;
  sub: Subscription;
  submited = false;
  sent = false;
  constructor(
    private cartService: CartService,
    private fb: FormBuilder,
    private snackBar: MatSnackBar,
    private orderService: OrderService,
    private router: Router
  ) {}

  ngOnInit() {
    if (!this.cartService.cartItems.length) {
      this.router.navigate(['error-page']);
    }
    this.totalPriceOrigin = this.cartService.totalPrice;
    this.totalPrice = this.totalPriceOrigin;
    this.totalQuantity = this.cartService.totalQuantity;
    this.form = this.fb.group({
      city: [''],
      street: [''],
      house: ['', CustomValidator.numberValidator],
      flat: ['', CustomValidator.numberValidator],
      info: [''],
      lastname: ['', Validators.required],
      name: ['', Validators.required],
      phone: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]]
    });
  }
  changeAddress(e) {
    this.address = e.checked ? 2 : 1;
  }
  setDeliveryType(number): void {
    this.deliveryType = number;
    if (this.deliveryType === 2) {
      this.totalPrice += this.deliveryPrice;
    } else {
      this.totalPrice = this.totalPriceOrigin;
    }
  }
  get f() {
    return this.form.controls;
  }
  onSubmit() {
    // console.log(this.form);
    if (!this.confirmed) {
      this.openSnackBar(
        'Подтвердите что ознакомлены с политикой конфиденциальности',
        'Закрыть'
      );
      return;
    }
    if (this.form.invalid) {
      this.openSnackBar('Исправьте ошибки', 'Закрыть');
      return;
    }
    const data = this.form.value;
    data['delivery_type'] = this.deliveryType;
    data['delivery_country'] = this.address;
    data['products'] = this.cartService.cartItems.map(p => {
      const res = {
        id: p.id,
        quantity: p.quantity
      };
      if (p.texture) {
        res['texture'] = p.texture.id;
      }
      if (p.color) {
        res['color'] = p.color.id;
      }
      return res;
    });
    this.submited = true;
    this.sub = this.orderService.sendOrder(data).subscribe(res => {
      let message: string;
      if (res.status === 200) {
        message = 'Ваш заказ принят!';
        this.cartService.emptyCart();
        this.sent = true;
      } else {
        message = res.error;
      }
      this.submited = false;
      this.openSnackBar(message, 'Закрыть');
    });
  }
  onConfirm(e) {
    this.confirmed = true;
  }
  openSnackBar(message: string, action: string, duration = 5000): void {
    this.snackBar.open(message, action, {
      duration: duration,
      horizontalPosition: 'center',
      verticalPosition: 'bottom'
    });
  }
}
