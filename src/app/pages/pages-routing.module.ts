import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PagesComponent } from './pages.component';
import { HomeComponent } from './home/home.component';
import { ArticleComponent } from './article/article.component';
import { ErrorComponent } from './error/error.component';
import { CategoryComponent } from './category/category.component';
import { CatalogComponent } from './catalog/catalog.component';
import { ProductComponent } from './product/product.component';
import { CartComponent } from './cart/cart.component';
import { OrderComponent } from './order/order.component';
import { FolioComponent } from './folio/folio.component';
import { VideoComponent } from './video/video.component';

const routes: Routes = [
  {
    path: '',
    component: PagesComponent,
    children: [
      { path: '', component: HomeComponent },
      { path: 'article/:slug', component: ArticleComponent },
      { path: 'category/:slug', component: CategoryComponent },
      { path: 'product/:slug', component: ProductComponent },
      { path: 'catalog/:slug', component: CatalogComponent },
      { path: 'catalog', component: CatalogComponent },
      { path: 'cart', component: CartComponent },
      { path: 'order', component: OrderComponent },
      { path: 'folio', component: FolioComponent },
      { path: 'video', component: VideoComponent },
      { path: '**', component: ErrorComponent },
      { path: 'error-page', component: ErrorComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule {}
