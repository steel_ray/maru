import { Component, OnInit } from '@angular/core';
import { CartService } from '../../share/services/cart.service';
import { Cart } from '../../share/models/cart.model';

@Component({
  selector: 'ln-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {
  items: Cart[];
  totalQuantity: number;
  totalPrice: number;
  constructor(private cartService: CartService) {}

  ngOnInit() {
    this.items = this.cartService.cartItems;
    this.totalQuantity = this.cartService.totalQuantity;
    this.totalPrice = this.cartService.totalPrice;
  }

  setQuantity(increase = true, cartItem: Cart) {
    // this.items.map(item => {
    //   if (increase) {
    //     return item.id === id ? (item.quantity = item.quantity + 1) : item;
    //   } else if (item.quantity > 1) {
    //     return item.id === id ? (item.quantity = item.quantity - 1) : item;
    //   }
    // });
    const index = this.cartService.itemIssets(cartItem);
    if (increase) {
      this.items[index].quantity = this.items[index].quantity + 1;
    } else if (this.items[index].quantity > 1) {
      this.items[index].quantity = this.items[index].quantity - 1;
    }
    localStorage.setItem('cart', JSON.stringify(this.items));
    this.totalPrice = this.cartService.totalPrice;
    this.totalQuantity = this.cartService.totalQuantity;
  }
  removeFromCart(item) {
    this.cartService.removeFromCart(item);
    this.items = this.cartService.cartItems;
    this.totalPrice = this.cartService.totalPrice;
    this.totalQuantity = this.cartService.totalQuantity;
  }
}
