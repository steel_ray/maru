import {
  Component,
  OnInit,
  OnDestroy,
  ViewChild,
  ElementRef
} from '@angular/core';
import { Product, Texture, Category } from '../../share/inverfaces/product.int';
import { Subscription, Observable, combineLatest } from 'rxjs';
import { ProductService } from '../../share/services/product.service';
import { ActivatedRoute, Router } from '@angular/router';
import { HeaderImagesService } from '../../share/services/header-images.service';
import { Cart } from '../../share/models/cart.model';
import { CartService } from '../../share/services/cart.service';
import { MatSnackBar } from '@angular/material/snack-bar';
declare var $: any;
@Component({
  selector: 'ln-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit, OnDestroy {
  product: Product;
  slug = '';
  sub: Subscription;
  routeSub: Subscription;
  productTexture = null;
  productColor = null;
  textures: Texture[];
  texturesOrigin: Texture[];
  listExposed = false;
  listSize = 3;
  similarProducts: Observable<Array<Product>>;
  categories: Category[];
  categorySlug: string;
  quantity = 1;
  price: number;
  colorsGroups = [];
  isset = false; // product isset
  added = false; // product added
  selectedImage = 'main_img';
  @ViewChild('swiperCont') swiperCont: ElementRef;
  constructor(
    private productService: ProductService,
    private route: ActivatedRoute,
    private router: Router,
    private headerImagesService: HeaderImagesService,
    private cartService: CartService,
    private snackBar: MatSnackBar
  ) {
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
  }

  ngOnInit() {
    this.routeSub = this.route.params.subscribe(params => {
      this.slug = params['slug'];
    });

    const combined = combineLatest(
      this.productService.getProduct(this.slug),
      this.productService.getCategories()
    );

    this.sub = combined.subscribe(res => {
      if (res[0]['error'] !== undefined) {
        this.router.navigateByUrl('/error-page');
        return;
      }
      this.product = res[0];
      // this.selectedImage = this.product.thumbnail;
      if (this.product.colors) {
        this.colorsGroups = Object.keys(this.product.colors);
      }
      this.categories = res[1];
      this.categorySlug = this.product.category.slug;
      this.price = this.product.price;

      if (this.product.textures.length) {
        if (!this.productTexture) {
          const chosen = this.product.textures.find(t => t.chosen === 1);
          // console.log(this.addedToCart);
          this.productTexture = chosen ? chosen : this.product.textures[0];
        }
        this.texturesOrigin = this.product.textures;
        this.sliceList();
      }
      // console.log(this.texturesOrigin);
      this.countQuantityPriceFromCart();
      this.similarProducts = this.productService.getProducts({
        category_slug: this.product.category.slug,
        skip: this.product.slug,
        limit: 5,
        order_rand: true
      });
      // tslint:disable-next-line:max-line-length
      // setTimeout to avoid debug error like "ERROR Error: "ExpressionChangedAfterItHasBeenCheckedError: Expression has changed after it was checked"
      setTimeout(() => {
        this.headerImagesService.emitChange(
          this.product.category.images ? this.product.category.images : null
        );
        this.initCarousel();
      });
      this.setIssetStatus();
    });
  }

  onChangeImage(image: string): void {
    // console.log(this.product.thumbs[image]);
    this.selectedImage = image;
  }

  setIssetStatus() {
    if (this.productTexture) {
      this.isset = this.productTexture.status ? true : false;
    } else {
      this.isset = this.product.isset ? true : false;
    }
    // console.log(this.productTexture);
  }

  countQuantityPriceFromCart(): void {
    this.price = this.quantity * this.product.price;
  }

  resetSelectedData(): void {
    this.quantity = 1;
    this.price = this.product.price;
    this.productTexture = this.product.textures
      ? this.product.textures[0]
      : null;
    this.productColor = null;
  }

  ngOnDestroy(): void {
    if (this.sub) {
      this.sub.unsubscribe();
    }
  }
  goToCatalog(slug: string): void {
    slug = slug === 'all' ? '' : slug;
    this.router.navigate(['/catalog', slug], { relativeTo: this.route });
  }
  exposeList(): void {
    this.listExposed = !this.listExposed;
    if (!this.listExposed) {
      this.initCarousel();
    }
    if (this.listExposed) {
      this.listSize = 14;
    } else {
      this.listSize = 3;
    }
    this.sliceList();
  }
  showMore(): void {
    this.listSize += 5;
    this.sliceList();
  }
  sliceList(): void {
    this.textures = this.texturesOrigin.slice(0, this.listSize);
  }
  changeTexture(texture: Texture) {
    this.added = false;
    this.productTexture = texture;
    if (this.listExposed) {
      this.exposeList();
    }
  }
  setQuantity(increase = true) {
    if (this.quantity === 1 && !increase) {
      return false;
    }
    this.quantity = increase ? this.quantity + 1 : this.quantity - 1;
    this.price = this.quantity * this.product.price;
  }

  onOrder() {
    const product: Cart = {
      id: this.product.id,
      slug: this.product.slug,
      title: this.product.title,
      vendor_code: this.product.vendor_code,
      price: this.product.price,
      quantity: this.quantity
    };
    if (this.product.textures.length) {
      product.texture = {
        id: this.productTexture.id,
        filename: this.productTexture.filename,
        status: this.productTexture.status,
        title: this.productTexture.title
      };
    }
    if (this.product.thumbnail) {
      product.thumbnail = this.product.thumbnail;
    }
    if (this.productColor) {
      product.color = this.productColor;
    }
    // console.log(this.cartService.itemIssets(product));
    if (this.cartService.itemIssets(product) < 0) {
      this.cartService.addToCart(product);
      this.added = true;
    } else {
      this.added = true;
      this.openSnackBar('Товар ранее был добавлен в корзину.', 'Закрыть');
    }
  }

  openSnackBar(message: string, action: string): void {
    this.snackBar.open(message, action, {
      duration: 5000,
      horizontalPosition: 'right',
      verticalPosition: 'top'
    });
  }

  onColorSelect(e: any) {
    this.added = false;
    const id = e.value;
    let color: any;
    Object.values(this.product.colors).map((c: any) => {
      c.map(v => {
        if (v.id === id) {
          color = v.html;
        }
      });
    });
    this.productColor = {
      id: id,
      color: color
    };
  }
  get cartQueryParams() {
    return {
      color_id: this.productColor ? this.productColor.id : null,
      texture_id: this.productTexture ? this.productTexture.id : null,
      product_id: this.product.id
    };
  }

  initCarousel() {
    if (!this.productTexture || this.texturesOrigin.length < 3) {
      return;
    }
    // console.log(this.texturesOrigin);
    setTimeout(() => {
      $(this.swiperCont.nativeElement).jCarouselLite({
        vertical: true,
        auto: 4000,
        speed: 1000,
        visible: 3
      });
    });
  }
}
