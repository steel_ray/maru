import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Post } from '../../share/inverfaces/post.int';
import { map, shareReplay } from 'rxjs/operators';
import { PostService } from '../../share/services/post.service';
import { ProductService } from '../../share/services/product.service';
import { Product, Category, Texture } from '../../share/inverfaces/product.int';

const CACHE_SIZE = 1;

@Injectable()
export class HomeService {
  slidesCache: Observable<Array<Post>>;
  newFromCache: Observable<Post>;
  productsCache: Observable<Array<Product>>;
  categoriesCache: Observable<Array<Category>>;
  texturesCache: Observable<Array<Texture>>;
  advantagesCache: Observable<Array<Post>>;
  constructor(
    private postService: PostService,
    private productService: ProductService
  ) {}
  get slides() {
    if (!this.slidesCache) {
      this.slidesCache = this.postService
        .getPosts({ category_slug: 'main-slider' })
        .pipe(shareReplay(CACHE_SIZE));
    }
    return this.slidesCache;
  }

  get newFromPost() {
    if (!this.newFromCache) {
      this.newFromCache = this.postService.getPost('new-from-maru').pipe(
        shareReplay(CACHE_SIZE),
        map(res => res)
      );
    }
    return this.newFromCache;
  }
  get products() {
    if (!this.productsCache) {
      this.productsCache = this.productService
        .getProducts({ expand: 'category' })
        .pipe(shareReplay(CACHE_SIZE));
    }
    return this.productsCache;
  }
  get categories() {
    if (!this.categoriesCache) {
      this.categoriesCache = this.productService
        .getCategories()
        .pipe(shareReplay(CACHE_SIZE));
    }
    return this.categoriesCache;
  }

  get textures() {
    if (!this.texturesCache) {
      this.texturesCache = this.productService
        .getTextures({
          limit: 15,
          order_rand: true,
          expand: 'product'
        })
        .pipe(shareReplay(CACHE_SIZE));
    }
    return this.texturesCache;
  }

  get advantages() {
    if (!this.advantagesCache) {
      this.advantagesCache = this.postService
        .getPosts({
          category_slug: 'our-advantages',
          limit: 4
        })
        .pipe(shareReplay(CACHE_SIZE));
    }
    return this.advantagesCache;
  }
}
