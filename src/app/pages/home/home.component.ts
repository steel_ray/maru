import {
  Component,
  OnInit,
  OnDestroy,
  ViewChild,
  ElementRef
} from '@angular/core';
import { Post } from '../../share/inverfaces/post.int';
import { Subscription } from 'rxjs';
import { HomeService } from './home.service';
import { combineLatest, interval } from 'rxjs';
import { forEach } from 'lodash';
import { Product, Category, Texture } from '../../share/inverfaces/product.int';
@Component({
  selector: 'ln-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, OnDestroy {
  isLoaded = false;
  mainSliderItems: Array<Post>;
  sub: Subscription;
  intervalSub: Subscription;
  newFromMaru: Post;
  products: Array<Product>;
  productsOrigin: Array<Product>;
  categories: Array<Category>;
  textures: Array<Texture>;
  advantages: Array<Post>;
  @ViewChild('msImgs', { read: ElementRef }) msImgs: ElementRef;
  @ViewChild('msPagination', { read: ElementRef }) msPagination: ElementRef;
  @ViewChild('msCounter', { read: ElementRef }) msCounter: ElementRef;
  @ViewChild('msDesc', { read: ElementRef }) msDesc: ElementRef;
  prodCarouselOptions = {
    margin: 111,
    nav: true,
    autoplay: true,
    rewind: true,
    autoplayHoverPause: true,
    lazyLoad: true,
    smartSpeed: 1200,
    // slideTransition: 4000,
    navText: [
      '<div class="nav-btn prev-slide"><i class="material-icons">navigate_before</i></div>',
      '<div class="nav-btn next-slide"><i class="material-icons">navigate_next</i></div>'
    ],
    responsiveClass: true,
    autoplaySpeed: 1200,
    responsive: {
      0: {
        items: 1,
        nav: true
      },
      600: {
        items: 1,
        nav: true
      },
      1000: {
        items: 3,
        nav: true,
        loop: false,
        margin: 80
      },
      1500: {
        items: 3,
        nav: true,
        loop: false
      }
    }
    // onChanged: function(event) {
    //   console.log(event);
    //   // const items = document.querySelectorAll('.production-list .owl-item.active');
    //   // console.log(items);
    // }
  };
  textureCarouselOptions = {
    margin: 52,
    nav: true,
    autoplay: true,
    rewind: true,
    autoplayHoverPause: true,
    lazyLoad: true,
    smartSpeed: 1000,
    autoplaySpeed: 1200,
    navText: [
      '<div class="nav-btn prev-slide"><i class="material-icons">navigate_before</i></div>',
      '<div class="nav-btn next-slide"><i class="material-icons">navigate_next</i></div>'
    ],
    responsiveClass: true,
    responsive: {
      0: {
        items: 1,
        nav: true
      },
      600: {
        items: 1,
        nav: true
      },
      1000: {
        items: 5,
        nav: true,
        loop: false
      },
      1440: {
        items: 6,
        nav: true,
        loop: false
      },
      1600: {
        items: 6,
        nav: true,
        loop: false
      }
    }
  };

  constructor(private homeService: HomeService) {}

  ngOnInit(): void {
    const combined = combineLatest(
      this.homeService.slides,
      this.homeService.newFromPost,
      this.homeService.products,
      this.homeService.categories,
      this.homeService.textures,
      this.homeService.advantages
    );
    this.sub = combined.subscribe(res => {
      this.mainSliderItems = res[0];
      this.newFromMaru = res[1];
      this.productsOrigin = res[2];
      this.products = this.productsOrigin;
      this.categories = res[3];
      this.textures = res[4];
      this.advantages = res[5];
      // console.log(this.newFromMaru);
      setTimeout(_ => {
        this.initMainSlider();
      }, 200);
      this.isLoaded = true;
    });
  }

  productsFilter(e) {
    this.products = null;
    setTimeout(() => {
      this.products =
        e.value === 'all'
          ? this.productsOrigin
          : this.productsOrigin.filter(p => p.category.slug === e.value);
    }, 100);
  }

  ngOnDestroy(): void {
    if (this.sub) {
      this.sub.unsubscribe();
    }
    if (this.intervalSub) {
      this.intervalSub.unsubscribe();
    }
  }

  initMainSlider(): void {
    const imgsItems = this.msImgs.nativeElement.children;
    const classNames = ['current-img', 'next-img', 'prev-img'];
    forEach(imgsItems, (v, k) => {
      if (k <= 2) {
        v.classList.add(classNames[k]);
      }
    });
    this.autoPlay();
  }
  autoPlay(): void {
    this.intervalSub = interval(4000).subscribe(() => {
      this.msNext();
    });
  }
  goToNext(): void {
    if (this.intervalSub) {
      this.intervalSub.unsubscribe();
    }
    this.msNext();
    this.autoPlay();
  }
  msNext(): void {
    const imgsItems = this.msImgs.nativeElement.children;
    const paginationItems = this.msPagination.nativeElement.children;
    const counterItems = this.msCounter.nativeElement.children[1].children;
    const descItems = this.msDesc.nativeElement.children;
    let currentImgIndex = 0;
    // if (typeof document !== 'undefined') {
    currentImgIndex = this.getElementIndex(
      document.querySelector('.current-img')
    );
    // }
    // Object.keys(imgsItems).forEach(key => {
    //   const item = imgsItems[key];
    //   if (typeof item !== 'undefined' && typeof window !== 'undefined') {
    //     Object.keys(item.classList).forEach(k => {
    //       if (item.classList[k] === 'current-img') {
    //         currentImgIndex = +key;
    //       }
    //     });
    //   }
    // });
    // console.log(currentImgIndex, currentImgIndex2);
    const activeImgIndex =
      currentImgIndex === imgsItems.length - 1 ? 0 : currentImgIndex + 1;
    const nextImgIndex =
      activeImgIndex === imgsItems.length - 1 ? 0 : activeImgIndex + 1;
    const prevImgIndex = currentImgIndex;
    forEach(imgsItems, (v, k) => {
      let className = 'hiding';
      switch (true) {
        case k === prevImgIndex:
          className = 'prev-img';
          break;
        case k === activeImgIndex:
          className = 'current-img';
          break;
        case k === nextImgIndex:
          className = 'next-img';
          break;
      }
      v.classList.remove('current-img');
      v.classList.remove('next-img');
      v.classList.remove('prev-img');
      v.classList.add(className);
      if (k !== activeImgIndex) {
        paginationItems[k].classList.remove('active');
        descItems[k].classList.remove('active');
        descItems[k].classList.remove('show-content');
        counterItems[k].classList.remove('active');
      } else {
        paginationItems[activeImgIndex].classList.add('active');
        descItems[activeImgIndex].classList.add('active');
        descItems[activeImgIndex].classList.add('show-content');
        counterItems[activeImgIndex].classList.add('active');
      }
    });
  }
  // select element index
  getElementIndex(node) {
    let index = 0;
    while ((node = node.previousElementSibling)) {
      index++;
    }
    return index;
  }
}
