import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { PostService } from '../../share/services/post.service';
import { Post } from '../../share/inverfaces/post.int';

@Component({
  selector: 'ln-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})
export class CategoryComponent implements OnInit, OnDestroy {
  slug: string;
  sub: Subscription;
  routerSub: Subscription;
  isLoaded = false;
  posts: Post[];
  constructor(
    private route: ActivatedRoute,
    private postService: PostService,
    private router: Router
  ) {
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
  }

  ngOnInit() {
    this.routerSub = this.route.params.subscribe(res => {
      this.slug = res['slug'];
    });
    this.sub = this.postService
      .getPosts({ category_slug: this.slug })
      .subscribe(res => {
        this.posts = res;
        if (!this.posts.length) {
          this.router.navigate(['error-page']);
        }
        this.isLoaded = true;
      });
  }
  ngOnDestroy(): void {
    if (this.sub) {
      this.sub.unsubscribe();
    }
    if (this.routerSub) {
      this.routerSub.unsubscribe();
    }
    this.isLoaded = false;
  }
}
