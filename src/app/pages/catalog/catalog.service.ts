import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Product, Category } from '../../share/inverfaces/product.int';
import { ProductService } from '../../share/services/product.service';
import { shareReplay } from 'rxjs/operators';

const CACHE_SIZE = 1;

@Injectable()
export class CatalogService {
  productsCache: Observable<Array<Product>>;
  categoriesCache: Observable<Array<Category>>;
  constructor(private productService: ProductService) {}

  get products() {
    if (!this.productsCache) {
      this.productsCache = this.productsRequest().pipe(shareReplay(CACHE_SIZE));
    }
    return this.productsCache;
  }
  productsRequest(): Observable<any> {
    return this.productService.getProducts({ expand: 'category' });
  }
  get categories() {
    if (!this.categoriesCache) {
      this.categoriesCache = this.categoriesRequest().pipe(
        shareReplay(CACHE_SIZE)
      );
    }
    return this.categoriesCache;
  }
  categoriesRequest(): Observable<any> {
    return this.productService.getCategories();
  }
}
