import {
  Component,
  OnInit,
  OnDestroy,
  ViewChild,
  ElementRef
} from '@angular/core';
import { Product, Category } from '../../share/inverfaces/product.int';
import { Subscription, from, combineLatest } from 'rxjs';
import { CatalogService } from './catalog.service';
import { groupBy, mergeMap, toArray } from 'rxjs/operators';
import { HeaderImagesService } from '../../share/services/header-images.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'ln-catalog',
  templateUrl: './catalog.component.html',
  styleUrls: ['./catalog.component.scss']
})
export class CatalogComponent implements OnInit, OnDestroy {
  products: Array<Array<Product>>;
  productsOrigin: Array<Product>;
  categories: Array<Category>;
  sub: Subscription;
  sub2: Subscription;
  routeSub: Subscription;
  isLoaded = false;
  slug = 'all';
  @ViewChild('productsList', { read: ElementRef }) productsList: ElementRef;
  @ViewChild('productsListCont', { read: ElementRef })
  productsListCont: ElementRef;
  constructor(
    private catalogService: CatalogService,
    private headerImgsService: HeaderImagesService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.routeSub = this.route.params.subscribe(params => {
      if (params['slug']) {
        this.slug = params['slug'];
      }
    });
    const combined = combineLatest(
      this.catalogService.products,
      this.catalogService.categories
    );
    this.sub = combined.subscribe(res => {
      this.productsOrigin = res[0];
      this.categories = res[1];
      this.productsFilter(this.slug);
    });
  }

  groupProducts(products: Array<Product>) {
    const result = [];
    const p = from(products);
    const r = p.pipe(
      groupBy(product => product.category.slug),
      mergeMap(group => group.pipe(toArray()))
    );
    this.sub2 = r.subscribe((group: any) => {
      group.sort = group[0].category.sort;
      // console.log(group);
      result.push(group);
    });
    this.isLoaded = true;
    this.setListHeight();
    result.sort((a, b) => {
      return a.sort - b.sort;
    });
    // console.log(result);
    return result;
  }

  setListHeight(): void {
    setTimeout(() => {
      const productList = this.productsList.nativeElement;
      const height = productList ? productList.clientHeight : 0;
      const cont: any = this.productsListCont.nativeElement;
      if (cont) {
        cont.style.height = `${height}px`;
      }
    }, 1);
  }

  onCategoryChange(slug: string) {
    this.productsFilter(slug);
    slug = slug === 'all' ? '' : slug;
    this.router.navigate(['/catalog', slug], {
      relativeTo: this.route
    });
  }

  productsFilter(slug) {
    if (this.sub2) {
      this.sub2.unsubscribe();
    }
    const category = this.categories.find(c => c.slug === slug);
    if (slug !== 'all' && !category) {
      this.router.navigateByUrl('error-page', { skipLocationChange: true });
    }
    setTimeout(() => {
      this.products =
        slug !== 'all' && category
          ? this.groupProducts(
              this.productsOrigin.filter(p => p.category.slug === category.slug)
            )
          : this.groupProducts(this.productsOrigin);
      this.headerImgsService.emitChange(category ? category.images : null);
    }, 100);
  }

  ngOnDestroy(): void {
    if (this.sub) {
      this.sub.unsubscribe();
    }
    if (this.sub2) {
      this.sub.unsubscribe();
    }
  }
}
