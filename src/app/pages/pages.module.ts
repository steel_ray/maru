import { NgModule } from '@angular/core';
import { PagesComponent } from './pages.component';
import { HomeComponent } from './home/home.component';
import { HomeService } from './home/home.service';
import { PagesRoutingModule } from './pages-routing.module';
import { ArticleComponent } from './article/article.component';
import { ErrorComponent } from './error/error.component';
import { CategoryComponent } from './category/category.component';
import { IncludesModule } from './includes/inludes.module';
import { CatalogComponent } from './catalog/catalog.component';
import { ProductComponent } from './product/product.component';
import { CatalogService } from './catalog/catalog.service';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { HeaderImagesService } from '../share/services/header-images.service';
import { CartService } from '../share/services/cart.service';
import { CartComponent } from './cart/cart.component';
import { OrderComponent } from './order/order.component';
import { OrderService } from './order/order.service';
import { PagesService } from './pages.service';
import { FolioComponent } from './folio/folio.component';
import { GridClass } from './folio/gridclass.pipe';
import { VideoComponent } from './video/video.component';

@NgModule({
  declarations: [
    PagesComponent,
    HomeComponent,
    ArticleComponent,
    ErrorComponent,
    CategoryComponent,
    CatalogComponent,
    ProductComponent,
    CartComponent,
    OrderComponent,
    FolioComponent,
    GridClass,
    VideoComponent
  ],
  imports: [PagesRoutingModule, IncludesModule, InfiniteScrollModule],
  exports: [IncludesModule],
  providers: [
    HomeService,
    CatalogService,
    HeaderImagesService,
    CartService,
    OrderService,
    PagesService
  ]
})
export class PagesModule {}
