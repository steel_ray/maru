import { Injectable } from '@angular/core';
import { MediaService } from '../share/services/media.service';
import { PostService } from '../share/services/post.service';
import { Observable } from 'rxjs';
import { Media } from '../share/inverfaces/media.int';
import { Post } from '../share/inverfaces/post.int';
import { shareReplay } from 'rxjs/operators';
const CACHE_SIZE = 1;
@Injectable()
export class PagesService {
  folioImagesCache: Observable<Media[]>;
  reviewsCache: Observable<Post[]>;
  newsCache: Observable<Post[]>;
  headerImagesCache: Observable<any>;
  constructor(
    private postService: PostService,
    private mediaService: MediaService
  ) {}

  get folioImages() {
    if (!this.folioImagesCache) {
      this.folioImagesCache = this.mediaService
        .getAlbum('portfolio', 10)
        .pipe(shareReplay(CACHE_SIZE));
    }
    return this.folioImagesCache;
  }

  get headerImages() {
    if (!this.headerImagesCache) {
      this.headerImagesCache = this.mediaService
        .getAlbum('header-default-images', 3)
        .pipe(shareReplay(CACHE_SIZE));
    }
    return this.headerImagesCache;
  }

  get news() {
    if (!this.newsCache) {
      this.newsCache = this.postService
        .getPosts({ category_slug: 'news', limit: 4 })
        .pipe(shareReplay(CACHE_SIZE));
    }
    return this.newsCache;
  }

  get reviews() {
    if (!this.reviewsCache) {
      this.reviewsCache = this.postService
        .getPosts({ category_slug: 'reviews', limit: 4 })
        .pipe(shareReplay(CACHE_SIZE));
    }
    return this.reviewsCache;
  }
}
