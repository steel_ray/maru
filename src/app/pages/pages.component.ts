import { Component, OnInit, OnDestroy, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription, combineLatest } from 'rxjs';
import { Helpers } from '../base/helpers';
import { Media } from '../share/inverfaces/media.int';
import { Post } from '../share/inverfaces/post.int';
import { map } from 'rxjs/operators';
import {
  HeaderImages,
  HeaderImagesService
} from '../share/services/header-images.service';
import { PagesService } from './pages.service';

@Component({
  selector: 'ln-pages',
  templateUrl: './pages.component.html',
  styleUrls: ['./pages.component.scss']
})
export class PagesComponent extends Helpers implements OnInit, OnDestroy {
  sub: Subscription;
  folioImages: Array<Media>;
  reviews: Array<Post>;
  news: Array<Post>;
  isLoaded = false;
  isHome = true;
  routerSub: Subscription;
  headerImages: HeaderImages;
  headerImagesOrigin: HeaderImages;
  headerImagesVisible = false;
  headerImagesSub: Subscription;
  currentPage: string;
  folioCarouselOptions = {
    margin: 200,
    nav: true,
    autoplay: true,
    rewind: true,
    autoplayHoverPause: true,
    lazyLoad: true,
    smartSpeed: 1200,
    autoplaySpeed: 1200,
    navText: [
      '<div class="nav-btn prev-slide"><i class="material-icons">navigate_before</i></div>',
      '<div class="nav-btn next-slide"><i class="material-icons">navigate_next</i></div>'
    ],
    responsiveClass: true,
    // autoWidth: true,
    responsive: {
      0: {
        items: 1,
        nav: true
      },
      600: {
        items: 1,
        nav: true
      },
      1000: {
        items: 3,
        nav: true,
        loop: false
      },
      1500: {
        items: 3,
        nav: true,
        loop: false
      }
    },
    onInitialized: () => {
      this.findCentralItem();
    },
    onTranslated: () => {
      this.findCentralItem();
    }
  };
  reviewsCarouselOptions = {
    margin: 0,
    nav: true,
    dots: false,
    autoplayTimeout: 6000,
    autoplay: true,
    rewind: true,
    autoplayHoverPause: true,
    lazyLoad: true,
    smartSpeed: 1200,
    autoplaySpeed: 1200,
    navText: [
      '<div class="nav-btn prev-slide"><i class="material-icons">navigate_before</i></div>',
      '<div class="nav-btn next-slide"><i class="material-icons">navigate_next</i></div>'
    ],
    responsiveClass: true,
    // autoWidth: true,
    responsive: {
      0: {
        items: 1,
        nav: true
      },
      600: {
        items: 1,
        nav: true
      },
      1000: {
        items: 1,
        nav: true,
        loop: false
      },
      1500: {
        items: 1,
        nav: true,
        loop: false
      }
    }
  };
  findCentralItem(): void {
    const activeItems = this.elRef.nativeElement.querySelectorAll(
      '.folio-slider .owl-item.active'
    );
    if (activeItems.length === 3) {
      activeItems.forEach(item => {
        if (item !== activeItems[1]) {
          item.classList.remove('central');
        } else {
          item.classList.add('central');
        }
      });
    }
  }

  constructor(
    private pageService: PagesService,
    private router: Router,
    private headerImagesService: HeaderImagesService,
    private elRef: ElementRef
  ) {
    super();
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
  }

  ngOnInit(): void {
    this.currentPage = this.router.url;
    this.isHome = this.isHomeCheck(this.currentPage);
    this.setHIVisibility();

    const combined = combineLatest(
      this.pageService.folioImages.pipe(map(res => res['data'])),
      this.pageService.news,
      this.pageService.reviews,
      this.pageService.headerImages.pipe(map(res => res['data']))
    );
    this.sub = combined.subscribe((res: any) => {
      this.folioImages = res[0];
      this.news = res[1];
      this.reviews = res[2];
      if (res[3].length) {
        const images = { img1: '', img2: '', img3: '' };
        for (let i = 1; i <= res[3].length; i++) {
          images[`img${i}`] = res[3][i - 1].url;
        }
        this.headerImagesOrigin = images;
        this.headerImages = this.headerImagesOrigin;
      }
      this.isLoaded = true;
      this.setHIVisibility();
    });
    this.headerImagesSub = this.headerImagesService.changeEmited.subscribe(
      (images: HeaderImages) => {
        this.headerImages = images ? images : this.headerImagesOrigin;
      }
    );
  }
  ngOnDestroy(): void {
    if (this.sub) {
      this.sub.unsubscribe();
    }
    if (this.routerSub) {
      this.routerSub.unsubscribe();
    }
    if (this.headerImagesSub) {
      this.headerImagesSub.unsubscribe();
    }
  }
  setHIVisibility(): void {
    if (!this.isHome) {
      setTimeout(() => {
        this.headerImagesVisible = true;
      }, 500);
    } else {
      this.headerImagesVisible = false;
    }
  }
}
