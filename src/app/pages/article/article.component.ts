import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { Post } from '../../share/inverfaces/post.int';
import { PostService } from '../../share/services/post.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'ln-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.scss']
})
export class ArticleComponent implements OnInit, OnDestroy {
  routerSub: Subscription;
  sub: Subscription;
  isLoaded = false;
  post: Post;
  slug: string;
  constructor(
    private postService: PostService,
    private route: ActivatedRoute,
    private router: Router
  ) {
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
  }

  ngOnInit() {
    this.routerSub = this.route.params.subscribe(params => {
      this.slug = params['slug'];
    });
    this.sub = this.postService.getPost(this.slug).subscribe(res => {
      if (res['status'] && res['status']) {
        this.router.navigate(['error-page']);
      }
      this.post = res;
      this.isLoaded = true;
    });
  }
  ngOnDestroy() {
    if (this.sub) {
      this.sub.unsubscribe();
    }
    if (this.routerSub) {
      this.routerSub.unsubscribe();
    }
    this.isLoaded = false;
  }
}
