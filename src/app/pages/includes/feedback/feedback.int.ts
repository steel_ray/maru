export interface Feedback {
  name: string;
  lastname: string;
  phone: string;
  email: string;
  message: string;
}

export interface FeedbackResponse {
  data: string;
  status: number;
  error: any;
}
