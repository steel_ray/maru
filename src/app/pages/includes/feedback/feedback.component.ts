import {
  Component,
  OnInit,
  ViewEncapsulation,
  Input,
  Output,
  EventEmitter,
  OnDestroy
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FeedbackService } from './feedback.service';
import { Feedback, FeedbackResponse } from './feedback.int';
import { Subscription } from 'rxjs';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'ln-feedback',
  templateUrl: './feedback.component.html',
  styleUrls: ['./feedback.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class FeedbackComponent implements OnInit, OnDestroy {
  form: FormGroup;
  submited = false;
  sub: Subscription;
  @Input() feedbackOpened = false;
  @Output() success: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() hideFeedback: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor(
    private fb: FormBuilder,
    private feedbackService: FeedbackService,
    private snackBar: MatSnackBar
  ) {}

  ngOnInit() {
    this.form = this.fb.group({
      name: [
        '',
        [Validators.required, Validators.minLength(2), Validators.maxLength(30)]
      ],
      lastname: ['', [Validators.minLength(2), Validators.maxLength(30)]],
      phone: ['', [Validators.minLength(5), Validators.maxLength(25)]],
      email: ['', [Validators.required, Validators.email]],
      message: [
        '',
        [
          Validators.required,
          Validators.minLength(5),
          Validators.maxLength(1024)
        ]
      ]
    });
  }

  ngOnDestroy(): void {
    if (this.sub) {
      this.sub.unsubscribe();
    }
  }

  get f() {
    return this.form.controls;
  }

  onSubmit(): void {
    if (!this.form.invalid) {
      this.submited = true;
      const data: Feedback = this.form.value;
      this.feedbackService.send(data).subscribe((res: FeedbackResponse) => {
        this.submited = false;
        this.openSnackBar('Сообщение отправлено!', 'Закрыть');
        this.success.emit(true);
        this.form.reset();
      });
    }
  }

  openSnackBar(message: string, action: string): void {
    this.snackBar.open(message, action, {
      duration: 5000,
      horizontalPosition: 'right',
      verticalPosition: 'top'
    });
  }
  toggleFeedback() {
    this.hideFeedback.emit(true);
  }
}
