import { Component, Input } from '@angular/core';
import { MenuModel } from './menu.model';
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA
} from '@angular/material/dialog';
import { AddressDialogComponent } from '../address-dialog/address-dialog.component';
import { Post } from '../../../share/inverfaces/post.int';

@Component({
  selector: 'ln-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent {
  @Input() menu: MenuModel;
  @Input() address: Post[];
  constructor(public dialog: MatDialog) {}
  openDialog(): void {
    const dialogRef = this.dialog.open(AddressDialogComponent, {
      width: '800px',
      panelClass: 'address-dialog',
      data: this.address
    });
  }
}
