import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { MenuModel } from './menu.model';
import { HttpClient } from '@angular/common/http';
import { ApiBase } from '../../../base/api-base.service';
import { map } from 'rxjs/operators';

@Injectable()
export class MenuService extends ApiBase {
  constructor(public http: HttpClient) {
    super(http);
  }
  getMenu(): Observable<MenuModel> {
    return this.get('/menu-items/main-menu').pipe(map(data => data['data']));
  }
}
