import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'menuLink'
})

export class MenuLinkPipe implements PipeTransform {
  transform(url: string) {
    return url.charAt(0) === '/' ? url : '/category/' + url;
  }
}
