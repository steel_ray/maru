import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { PostService } from '../../../share/services/post.service';
import { Post } from '../../../share/inverfaces/post.int';
import { Subscription } from 'rxjs';

@Component({
  selector: 'ln-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  constructor(private postService: PostService) {}
  year = new Date().getFullYear();
  address: Post;
  contacts: Post;
  nets: Post;
  sub: Subscription;
  @Output() askQuestion: EventEmitter<boolean> = new EventEmitter<boolean>();
  ngOnInit(): void {
    this.sub = this.postService
      .getPosts({ category_slug: 'footer-blocks', expand: 'content' })
      .subscribe((res: any) => {
        this.address = res.find(post => post['slug'] === 'footer-address');
        this.contacts = res.find(post => post['slug'] === 'footer-contacts');
        this.nets = res.find(post => post['slug'] === 'footer-nets');
      });
  }
  toggleFeedback() {
    this.askQuestion.emit(true);
  }
}
