import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  OnDestroy
} from '@angular/core';
import { MenuModel } from '../menu/menu.model';
import { Subscription, combineLatest } from 'rxjs';
import { MenuService } from '../menu/menu.service';
import { Locale } from '../../../share/inverfaces/locale.int';
import { CartService } from '../../../share/services/cart.service';
import { Router, NavigationEnd } from '@angular/router';
import { Helpers } from '../../../base/helpers';
import { Post } from '../../../share/inverfaces/post.int';
import { PostService } from '../../../share/services/post.service';

@Component({
  selector: 'ln-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent extends Helpers implements OnInit, OnDestroy {
  langNavOpened = false;
  @Input() feedbackOpened = false;
  @Output() feedbackClosed: EventEmitter<boolean> = new EventEmitter<boolean>();
  menu: MenuModel;
  sub: Subscription;
  isLoaded = false;
  locales: Locale[] = [];
  address: Post[];
  activeLocale: Locale = {
    name: 'Ru',
    locale: 'ru'
  };
  menuOpened = false;
  cartCount: any;
  routerSub: Subscription;
  constructor(
    private menuService: MenuService,
    private cartService: CartService,
    private router: Router,
    private postService: PostService
  ) {
    super();
  }

  ngOnInit() {
    this.locales = this.getSetting('locales');
    this.routerSub = this.router.events.subscribe((e: any) => {
      if (e instanceof NavigationEnd) {
        this.menuOpened = false;
        this.feedbackOpened = false;
      }
      if (e instanceof NavigationEnd && typeof window !== 'undefined') {
        window.scroll(0, 0);
      }
    });
    if (localStorage.getItem('locale') && this.locales) {
      this.activeLocale = this.locales.find(
        l => l.locale === localStorage.getItem('locale')
      );
    }
    const combined = combineLatest(
      this.menuService.getMenu(),
      this.postService.getPosts({
        category_slug: 'contacts',
        expand: 'content'
      })
    );
    this.sub = combined.subscribe(res => {
      this.menu = res[0];
      this.address = res[1];
      // console.log('TCL: HeaderComponent -> ngOnInit -> this.menu', this.menu);
      this.isLoaded = true;
    });
    const cart = localStorage.getItem('cart');
    if (cart) {
      this.cartCount = JSON.parse(cart).length;
    }
    this.cartService.changeEmited.subscribe(number => {
      this.cartCount = number;
    });
  }
  ngOnDestroy(): void {
    if (this.sub) {
      this.sub.unsubscribe();
    }
    if (this.routerSub) {
      this.routerSub.unsubscribe();
    }
  }

  toggleLangNav(): void {
    this.langNavOpened = !this.langNavOpened;
  }

  toggleFeedback(): void {
    this.feedbackOpened = !this.feedbackOpened;
    // console.log(this.feedbackOpened);
    if (!this.feedbackOpened) {
      this.feedbackClosed.emit(true);
    }
  }

  changeLocale(locale: string) {
    const check = this.locales.find(l => l.locale === locale);
    if (!check) {
      return false;
    }
    this.activeLocale = check;
    localStorage['locale'] = locale;
    window.location.reload();
    // this.router.navigate([this.router.url]);
  }
  toggleMenu(): void {
    this.menuOpened = !this.menuOpened;
  }
}
