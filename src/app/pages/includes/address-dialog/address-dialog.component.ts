import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Post } from '../../../share/inverfaces/post.int';

@Component({
  selector: 'ln-address-dialog',
  templateUrl: './address-dialog.component.html',
  styleUrls: ['./address-dialog.component.scss']
})
export class AddressDialogComponent {
  activeAddress = 1;
  constructor(
    public dialogRef: MatDialogRef<AddressDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Post[]
  ) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

  toggleAddress(): void {
    this.activeAddress = this.activeAddress === 1 ? 2 : 1;
  }
}
