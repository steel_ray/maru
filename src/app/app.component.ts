import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { environment } from '../environments/environment';
import { Locale } from './share/inverfaces/locale.int';
import { AppService } from './app.service';

@Component({
  selector: 'ln-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  sub: Subscription;
  private defaultLang = environment.defaultLang; // default language of the app
  isLoaded = false;
  locales: Locale[] = [];
  feedbackOpened = false;
  constructor(private appService: AppService) {}

  ngOnInit(): void {
    this.sub = this.appService.getSettings().subscribe((res: any) => {
      // this.locales = res.locales;
      // delete res.locales;
      localStorage.setItem('settings', JSON.stringify(res));
      this.setDefaultLang();
      this.isLoaded = true;
    });
  }

  ngOnDestroy(): void {
    if (this.sub) {
      this.sub.unsubscribe();
    }
  }

  toggleFeedback(e: boolean) {
    this.feedbackOpened = !e;
  }

  setDefaultLang(): void {
    if (
      !localStorage.getItem('locale') ||
      (this.locales &&
        !this.locales.find(l => l.locale === localStorage.getItem('locale')))
    ) {
      localStorage.setItem('locale', this.defaultLang);
    }
  }
}
