export class Cart {
  constructor(
    public id: number,
    public title: string,
    public vendor_code: string,
    public price: number,
    public quantity: number,
    public slug: string,
    public thumbnail?: string,
    public texture?: {
      id: number;
      filename: string;
      status: number;
      title?: string;
    },
    public color?: {
      id: number;
      color: string;
    }
  ) {}
}
