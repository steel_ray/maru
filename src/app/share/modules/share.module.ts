import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MyPipesModule } from './my-pipes.module';
import { MyMaterialsModule } from './my-materials.module';
import { OwlModule } from 'ngx-owl-carousel';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    MyPipesModule,
    MyMaterialsModule,
    OwlModule,
    RouterModule
  ],
  exports: [
    CommonModule,
    CommonModule,
    MyPipesModule,
    MyMaterialsModule,
    OwlModule,
    RouterModule
  ]
})
export class ShareModule {}
