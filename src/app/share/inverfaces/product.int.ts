export interface Category {
  title: string;
  slug: string;
  images: {
    img1: string;
    img2: string;
    img3: string;
  };
}
export interface Texture {
  id: number;
  filename: string;
  chosen: number;
  product: { slug: string; title: string };
  status: number;
  title?: string;
}
export interface Color {
  title: string;
  group_title: string;
  rgb: string;
  id: number;
  ral: number;
}
export interface Product {
  title: string;
  price: number;
  slug: string;
  new: number;
  thumbnail: string;
  category: Category;
  id: number;
  has_color: any;
  vendor_code?: string;
  isset: number;
  images: {
    img1: string;
    img2: string;
    img3: string;
    main_img: string;
  };
  thumbs?: any;
  textures?: Array<Texture>;
  colors?: Array<Color>;
}

export interface ProductResponse {
  data: Array<Product>;
  status: number;
  error: any;
}
export interface CategoryResponse {
  data: Array<Category>;
  status: number;
  error: any;
}
export interface TextureResponse {
  data: Array<Texture>;
  status: number;
  error: any;
}
