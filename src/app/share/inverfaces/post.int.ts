export interface Post {
  title: string;
  description: string;
  slug: string;
  thumbnail: string;
  published_at: number;
  category_title: string;
}

export interface PostResponse {
  status: number;
  error: Array<any>;
  data: Array<Post>;
}
