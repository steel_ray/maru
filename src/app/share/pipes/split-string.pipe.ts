import { Pipe, PipeTransform } from '@angular/core';
import { Helpers } from '../../base/helpers';

@Pipe({
  name: 'splitString'
})

export class SplitString extends Helpers implements PipeTransform {
  transform(value: string) {
    return value !== undefined ? this.splitString(value) : '';
  }
}
