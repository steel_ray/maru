import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

export interface HeaderImages {
  img1: String;
  img2: String;
  img3: String;
}

@Injectable()
export class HeaderImagesService {
  private emitChangeSource = new Subject<HeaderImages>();
  changeEmited = this.emitChangeSource.asObservable();
  emitChange(images: HeaderImages) {
    this.emitChangeSource.next(images);
  }
}
