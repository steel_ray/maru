import { Injectable } from '@angular/core';
import { ApiBase } from '../../base/api-base.service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Media } from '../inverfaces/media.int';

@Injectable()
export class MediaService extends ApiBase {
  constructor(public http: HttpClient) {
    super(http);
  }
  getAlbum(slug: string, limit = 5): Observable<Media[]> {
    return this.get(`/get-album/${slug}?limit=${limit}`);
  }
}
