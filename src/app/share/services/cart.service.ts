import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Cart } from '../models/cart.model';

@Injectable()
export class CartService {
  private emitChangeSource = new Subject();
  changeEmited = this.emitChangeSource.asObservable();
  emitChange(number: number) {
    this.emitChangeSource.next(number);
  }

  addToCart(product: Cart) {
    const cart: Cart[] = this.cartItems;
    cart.push(product);
    localStorage.setItem('cart', JSON.stringify(cart));
    this.shareCartCount();
  }
  removeFromCart(item) {
    const cart = this.cartItems;
    const index = cart.findIndex(
      p => JSON.stringify(p) === JSON.stringify(item)
    );
    if (index !== -1) {
      cart.splice(index, 1);
    }
    if (!cart.length) {
      localStorage.removeItem('cart');
    } else {
      localStorage.setItem('cart', JSON.stringify(cart));
    }
    this.shareCartCount();
  }
  get cartItems() {
    const cart = JSON.parse(localStorage.getItem('cart'));
    return !cart ? [] : cart;
  }
  get totalQuantity() {
    let quantity = 0;
    this.cartItems.map(item => {
      quantity += item.quantity;
    });
    return quantity;
  }
  get totalPrice() {
    let price = 0;
    this.cartItems.map(item => {
      price += item.quantity * item.price;
    });
    return price;
  }
  shareCartCount(): void {
    this.emitChange(this.cartItems.length);
  }
  itemIssets(item: Cart) {
    const items: Cart[] = this.cartItems;
    let itemIndex = -1;
    if (items) {
      if (item.texture && item.color) {
        items.forEach((n, index) => {
          if (
            n.id === item.id &&
            n.texture &&
            n.texture.id === item.texture.id &&
            n.color &&
            n.color.id === item.color.id
          ) {
            // console.log(n);
            // console.log(item);
            itemIndex = index;
          }
        });
      } else if (item.texture && !item.color) {
        items.forEach((n, index) => {
          if (
            n.id === item.id &&
            n.texture &&
            n.texture.id === item.texture.id &&
            !n.color
          ) {
            itemIndex = index;
          }
        });
      } else if (!item.texture && item.color) {
        items.forEach((n, index) => {
          if (
            n.id === item.id &&
            n.color &&
            n.color.id === item.color.id &&
            !n.texture
          ) {
            itemIndex = index;
          }
        });
      } else if (!item.texture && !item.color) {
        itemIndex = items.findIndex(n => n.id === item.id);
      }

      // const item = items.find(p => p.id === id);
    }
    return itemIndex;
  }
  emptyCart() {
    localStorage.removeItem('cart');
    this.shareCartCount();
  }
}
