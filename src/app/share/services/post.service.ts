import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiBase } from '../../base/api-base.service';
import { Observable } from 'rxjs';
import { Post } from '../inverfaces/post.int';

@Injectable()
export class PostService extends ApiBase {
  constructor(public http: HttpClient) {
    super(http);
  }
  getPost(slug: string): Observable<Post> {
    return this.get(`/posts/${slug}`);
  }
  getPosts(
    params: {
      page?: number;
      limit?: number;
      order_rand?: number;
      category_slug?: string;
      expand?: string;
    } = {}
  ): Observable<Post[]> {
    return this.get(`/posts`, params);
  }
}
